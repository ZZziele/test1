//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

import java.io.*;
import java.util.Random;
import java.util.Scanner;
import java.util.zip.ZipEntry;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        ///////zadanie 1 /////////////////////////
        int[] var10001 = new int[]{1, 2, 3, 4, 5, 12, 77};
        System.out.println("" + suma(var10001));
        ///////zadanie 2 ///////////////////////////
        System.out.println(lacz("ww", "siema", "Polska", "Szymek"));

        //////zadanie 3///////////////////////////////////////
        int[] tab1 = new int[]{1, 2, 3};
        int[] tab2 = new int[]{4, 5, 6};
        System.out.println(liczbaElementow(tab1, tab2));

        //// zadanie 4////////////////////////////////////////
        System.out.println(max(-7, -2, -1, -11));

        ////zadanie 5//////////////////////////////////////////
        String nazwaPliku = "src/plik.txt";
        System.out.println("Suma Par liczb z pliku");
        wypiszTablice(wczytajPlikDodaj(nazwaPliku));


        ///zadanie 6///////////////////////////////////////
        wypiszTablice(wczytajPlikWypisz(nazwaPliku));
        // Pytanie czy jest jakiś sposób by zrobić od początku tablice równą długośći lini w pliku?
        // czy najpierw żeby poznać rozmiar tabeli którą chcemy stworzyc musimy wpieprw przeskanowac plik
        // np jakaś funkcją skanuj plik która zwraca ilość lini

        // Zadanie 7 //////////////////////////////////////////////
      /*  String nazwaPliku2 = "src/plik2.txt";
        System.out.println("Ilośc stringów w pliku 2 ");
        System.out.println("" + wczytajPlikZliczStringi(nazwaPliku2));*/

        // Zadanie 8 /////////////////////////////////////////////
        /*String nazwaPliku3 = "src/plik3.txt";
        System.out.println("Ilośc stringów w pliku 2 ");
        System.out.println("" + wczytajPlikSzukajStringa(nazwaPliku3,"Ferrari"));*/

        // Zadanie 9 ///////////////////////////////////////////////////

       //  zapiszPlik2("src/plik4.txt");

        // Zadanie 10 /////////////////////////////////////////////////////
        zapiszPlikWspołrzędne("src/plik5.txt");

        // Zadanie 11 /////////////////////////////////////////////////////

    }




    /////////////////Zadanie 1 ///////////////////////////////////////////
    static int suma(int... liczba) {
        int suma = 0;

        for(int i = 0; i < liczba.length; ++i) {
            suma += liczba[i];
        }

        return suma;
    }
    /////////////////Zadanie 1 ///////////////////////////////////////////
    /////////////////Zadanie 2 ///////////////////////////////////////////
    static String lacz(String... napis) {
        String suma = "";

        for(int i = 0; i < napis.length; ++i) {
            suma = suma.concat(napis[i]);
        }

        return suma;
    }
    /////////////////Zadanie 2 ///////////////////////////////////////////
    /////////////////Zadanie 3 ///////////////////////////////////////////
    static int liczbaElementow(int[]... tab) {
        int liczba = 0;

        for(int i = 0; i < tab.length; ++i) {
            for(int j = 0; j < tab[i].length; ++j) {
                ++liczba;
            }
        }

        return liczba;
    }
    /////////////////Zadanie 3 ///////////////////////////////////////////
    /////////////////Zadanie 4 ///////////////////////////////////////////
    static int max(int... tab) {
        int liczba = tab[0];

        for(int i = 0; i < tab.length; ++i) {
            if (tab[i] > liczba) {
                liczba = tab[i];
            }
        }

        return liczba;
    }
    /////////////////Zadanie 4 ///////////////////////////////////////////
    /////////////////Zadanie 5 ///////////////////////////////////////////
    static int[] wczytajPlikDodaj(String nazwaPliku) {
        File file = new File(nazwaPliku);
        int[] tablicaWynikowa = new int[8];

        try {
            Scanner scanner = new Scanner(file);

            for(int i = 0; scanner.hasNextLine(); ++i) {
                tablicaWynikowa[i] = scanner.nextInt()+ scanner.nextInt();
            }

            return tablicaWynikowa;
        } catch (FileNotFoundException var5) {
            throw new RuntimeException(var5);
        }
    }

    static void wypiszTablice(String[] tablica) {
        for(int i = 0; i < tablica.length; ++i) {
            System.out.println(tablica[i]);
        }

    }

    static void wypiszTablice(int[] tablica) {
        for(int i = 0; i < tablica.length; ++i) {
            System.out.println(tablica[i]);
        }


    }

    /////////////////Zadanie 5 ///////////////////////////////////////////
    /////////////////Zadanie 6 ///////////////////////////////////////////
    static String[] wczytajPlikWypisz(String nazwaPliku) {
        File file = new File(nazwaPliku);
        String[] tablicaWynikowa = new String[8];

        try {
            Scanner scanner = new Scanner(file);

            for(int i = 0; scanner.hasNextLine(); ++i) {
                tablicaWynikowa[i] = scanner.nextLine();
            }

            return tablicaWynikowa;
        } catch (FileNotFoundException var5) {
            throw new RuntimeException(var5);
        }
    }
    /////////////////Zadanie 6 ///////////////////////////////////////////
    /////////////////Zadanie 7 ///////////////////////////////////////////
    static int wczytajPlikZliczStringi(String nazwaPliku){
        File file = new File(nazwaPliku);
        int ile = 0;
        try {
            Scanner scanner = new Scanner(file);

            for(int i = 0; scanner.hasNext(); i++) {
               System.out.println(scanner.next());
                ile++;
            }

            return ile;
        } catch (FileNotFoundException var5) {
            throw new RuntimeException(var5);
        }
    }
    /////////////////Zadanie 7 ///////////////////////////////////////////
    /////////////////Zadanie 8 ///////////////////////////////////////////
    static int wczytajPlikSzukajStringa(String nazwaPliku,String napis){
        File file = new File(nazwaPliku);
        int ile = 0;
        try {
            Scanner scanner = new Scanner(file);
            String kontener;
            for(int i = 0; scanner.hasNext(); i++) {
                kontener = scanner.next();
                if(kontener.equals(napis)){
                    System.out.println(kontener);
                    ile++;
                }
            }

            return ile;
        } catch (FileNotFoundException var5) {
            throw new RuntimeException(var5);
        }
    }
    /////////////////Zadanie 8 ///////////////////////////////////////////
    static void zapiszPlik(String nazwaPliku, String[] tablicaString){
        try {
            FileWriter fileWriter = new FileWriter(nazwaPliku); // w tym, miejscu tworzymy plik gdzieś na dysku
            for(int i = 0;i < tablicaString.length; i++){
                fileWriter.write(tablicaString[i] + "\n");

            }

            fileWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }

    static void zapiszPlik2(String nazwaPliku){

        Scanner scanner = new Scanner(System.in);
        try {
            FileWriter fileWriter = new FileWriter(nazwaPliku);


            fileWriter.write(scanner.nextLine());
            fileWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static void zapiszPlikWspołrzędne(String nazwaPliku){

        Scanner scanner = new Scanner(System.in);
        try {
            FileWriter fileWriter = new FileWriter(nazwaPliku);
            double [][] tab = new double[2][2];
            System.out.println("Podaj współrzędną x punktu A ");
            tab[0][0] = scanner.nextDouble();
            System.out.println("Podaj współrzędną y punktu A ");
            tab[0][1] = scanner.nextDouble();
            System.out.println("Podaj współrzędną x punktu B ");
            tab[1][0] = scanner.nextDouble();
            System.out.println("Podaj współrzędną y punktu B ");
            tab[1][1] = scanner.nextDouble();
            double odleglosc = Math.sqrt( Math.pow(tab[0][0] - tab[1][0],2)  +Math.pow(tab[0][1] - tab[1][1],2));
            fileWriter.write(odleglosc+ "");
            fileWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static void zapiszPlikLosowe(String nazwaPliku, int ilosc){
        try {
            FileWriter fileWriter = new FileWriter(nazwaPliku);
            Random random = new Random();
            int [] tab = new int[ilosc];
            int suma = 0;
            for(int i = 0;i < ilosc; i++){
               tab[i] = random.nextInt(101);
               suma = suma + tab[i];

            }
            fileWriter.write( (double)(suma/ilosc) +"" );
            fileWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }



}
