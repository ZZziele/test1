public class Human {
    //pola
    public int age;
    public double height;
    public String hairColour;

    //konstruktory
    public Human(){

    }

    public Human(int age, double height, String hairColour){
        this.age = age;
        this.height = height;
        this.hairColour = hairColour;
    }
}
