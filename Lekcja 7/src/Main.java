import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       // wczytywanie danych z pliku
        String nazwaPliku = "src/plik.txt";
        String[] tablicaInt = {"aa","bb","ww","cc","gg","fff","sdsd","sdsdsd"};
        zapiszPlik(nazwaPliku,tablicaInt);
        metoda2("USD",10,20,50,100);
       /* System.out.println(FunkcjaString("Siema"));
        System.out.println(Potega(5));
        metoda1("Kwadrat", 5, 5, 5, 5);
        metoda1("Trójkąt", 5, 7, 8);
        metoda1("Pięciokąt", 5,5,5,5,5);*/
        Human bartek = new Human(); //wywołanie konstruktora
        bartek.age = 27;
        bartek.height = 175.5;
        bartek.hairColour = "Blond";

        Human andrzej = new Human();
        andrzej.age = 45;
        andrzej.height = 195.7;
        andrzej.hairColour = "Czarne";
        System.out.println("Bartek ma: " + bartek.age + " , a Andrzej ma: " + andrzej.age + " lat");

        Human rumcajs = new Human(30, 160, "Siwe");
        System.out.println("Wiek Rumcajsa: " + rumcajs.age);


        // Shift + F6  żeby zamienić nazwe zmiennej we wszyskich miejscachż
       // String[] liczby =  wczytajPlik(nazwaPliku);
       // wypiszTablice(liczby);
        // alt + enter żeby wczytać biblioteke

        //programm, któr ma 2 metody pierwsz ma przyjmować jako parametr stringa i zwracaćpierwsza litere tego stringa jako string
        // druga metoda ma przyjkmować jako parametr inta i zwracać potęge
    }

    static String FunkcjaString (String napis){
        return napis.substring(0,1);
    }

    static int Potega(int x){
        return x*x;
    }
    static void metoda2 (String napis,int... banknot){
        float suma= 0 , wZlotowkach = 0;

        System.out.println(napis);
        for(int i = 0; i < banknot.length; i++){
            System.out.println(banknot[i]);
            suma= suma + banknot[i];
        }
        switch (napis){
            case "GBP":
                wZlotowkach= suma*5f;
                break;
            case "EURO":
                wZlotowkach= suma*4.9f;
                break;
            case "USD":
                wZlotowkach= suma*4f;
                break;
            default:
                System.out.println("Nie rozpoznano walury");

        }
        wZlotowkach = suma * 4.9f;
        System.out.println("suma wynosi" + suma + " " + napis);
        System.out.println("Suma w zlotowkach  " + wZlotowkach);
    }



    static String[] wczytajPlik (String nazwaPliku){
        File file = new File(nazwaPliku);
        String[] tablicaWynikowa = new String[8];
        try {
            Scanner scanner = new Scanner(file);
            int i = 0;
            while (scanner.hasNextLine()){
                String liczba2 = scanner.nextLine();
                tablicaWynikowa[i] = liczba2;
                i++;

            }
            scanner.close();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }

        return tablicaWynikowa;
    }

    static void wypiszTablice(String[] tablica){
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }
    }

    static void wypiszTablice(int[] tablica){
        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }
    }

    static void zapiszPlik(String nazwaPliku, String[] tablicaString){
        try {
           FileWriter fileWriter = new FileWriter(nazwaPliku); // w tym, miejscu tworzymy plik gdzieś na dysku
           for(int i = 0;i < tablicaString.length; i++){
               fileWriter.write(tablicaString[i] + "\n");

            }

           fileWriter.close();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }


    }
    static void przykladowaMetoda() {
        System.out.println("przykladowaMetoda");
    }

    static void przykladowaMetodaZParametrami(int liczba) {
        System.out.println("przykladowaMetodaZParametrami: " + liczba);
    }

    static int przykladowaMetodaZParametramiIMetodaZwracana(int liczba) {
        System.out.println("przykladowaMetodaZParametrami: " + liczba);
        return liczba + 1;
    }

    static int przykladowaMetodaZwracajaInta(){
        Random random = new Random();
        int rezultat = random.nextInt(10);
        return rezultat;
    }

    static void metoda1(String napis, int... boki){ //varargs podejamy zmienna z trzema kropkami wtedy możemy do
        // funkcji wysłać tyle zmiennych ile chcemy a program zrobi z nich tablice

        System.out.println("Nazwa figury: " + napis);
        for (int i = 0; i < boki.length; i++) {
            System.out.println("Nazwa boki: " + boki[i]);
        }
        System.out.println("-------------");

    }





}