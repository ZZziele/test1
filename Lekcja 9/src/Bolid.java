import java.util.Arrays;

public class Bolid {

    private String team;

    private SteeringWheel steeringWheel;

    private Wheel[] wheels;

    private Tank tank;

    public Bolid(String team, SteeringWheel steeringWheel, Wheel[] wheels, Tank tank) {
        this.team = team;
        this.steeringWheel = steeringWheel;
        this.wheels = wheels;
        this.tank = tank;
    }


    public Tank getTank() {
        return tank;
    }

    public void setTank(Tank tank) {
        this.tank = tank;
    }

    public String getTeam() {
        return team;
    }



    public SteeringWheel getSteeringWheel() {
        return steeringWheel;
    }

    public void setSteeringWheel(SteeringWheel steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    public Wheel[] getWheels() {
        return wheels;
    }

    public void setWheels(Wheel[] wheels) {
        this.wheels = wheels;
    }

    public void setWheel(SteeringWheel steeringWheel) {
        this.steeringWheel = steeringWheel;
    }

    public void setTeam(String team) {
        this.team = team;
    }
}
