package Model;

public class Car {


    private String engine;

    private String registrationNumber;

    private String [] trunk ;

    CarCard porsheCard = new CarCard();





    private double maxSpeed;

    private double distance = 0 ;

    private int cleanLinessIntiger  = 0;

    private String cleanliness  = "Czysty";


    public  Car(){};

    public Car(double maxSpeed, String engine){
        this.maxSpeed = maxSpeed;

        this.engine = engine;
    }

    public Car(double maxSpeed, String engine, String registrationNumber, String [] trunk, String name , String model ,String carReview){
        this.maxSpeed = maxSpeed;
        this.engine = engine;
        this.registrationNumber = registrationNumber;
        this.trunk = trunk;
        CarCard porsheCard = new CarCard(name,model,carReview);
    }



    public void wypiszDaneTechniczne(){
        System.out.println("Silnik: " +engine );

        System.out.println("Max speed: " +  getMaxSpeed());
        System.out.println("Tablica rejestracyjna" + getRegistrationNumber());
        System.out.println( "Bagażnik " + getTrunk() );

    }

    public void go(){
        distance++;
        String [] czystosc = {"Czysty","Przybrudzony","Zakurzony","Brudny"};

       /* if(distance > 8 ){
            cleanliness = "Brudny";
        } else if (distance > 5) {
            cleanliness = "Zakurzony";
        } else if(distance > 2 ){
            cleanliness = "Przybrudzony";
        }*/
        if(distance%3==0){
            if(cleanLinessIntiger < 3) {
                cleanLinessIntiger++;
            }
            cleanliness = czystosc[cleanLinessIntiger];
        }

    }


    public String getEngine() {
        return engine;
    }



    public double getMaxSpeed() {
        return maxSpeed;
    }

    public double getDistance() {
        return distance;
    }

    public String getCleanliness() {
        return cleanliness;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }



    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public int getCleanLinessIntiger() {
        return cleanLinessIntiger;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getTrunk() {
        String result = "";
        for (String trunkElement : this.trunk) {
            result = result + trunkElement + "  ";
            
        }
        return result;
    }

    public void setTrunk(String[] trunk) {
        this.trunk = trunk;
    }

    public void setCleanLinessIntiger(int cleanLinessIntiger) {
        this.cleanLinessIntiger = cleanLinessIntiger;
    }

    public void setCleanliness(String cleanliness) {
        this.cleanliness = cleanliness;
    }
}
