import java.util.Random;

public class Main {
    public static void main(String[] args) {

      // zadanie 1///////////////////////////////////////////////////
       int[][] zadanie1Tablica = generujTabliceInt(5,5);
        System.out.println("Oryginał");
       wypisz(zadanie1Tablica);

       // zadane 3/////////////////////////////////////////////////////
        /*int[][] zadanie2Tablica = new int [5][5];
        for (int i = 0; i < zadanie1Tablica.length; i++){
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
                zadanie2Tablica[i][j] = zadanie1Tablica[i][zadanie1Tablica[i].length-1 - j];
            }
        }
        System.out.println("");
        System.out.println("Odbicie względem y");
        wypisz(zadanie2Tablica);*/

        // zadanie 4////////////////////////////////////////////////
       /* int suma = 0;
        for (int i = 0; i < zadanie1Tablica.length; i++){
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
               suma = suma + zadanie1Tablica[i][j];
            }
        }
        System.out.println("suma = " + suma);*/

        // Zadanie 5 ////////////////////////////////////////////////////
        /*int[][] zadanie5Tablica = new int [5][5];



        for (int i = 0; i < zadanie1Tablica.length; i++){
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
                zadanie5Tablica[i][j] = zadanie1Tablica[zadanie1Tablica.length - 1 - i][j];
            }
        }
        System.out.println("");
        System.out.println("Odbicie względem x");
        wypisz(zadanie5Tablica);*/

        // zadanie 6///////////////////////////////////////////////////////

        /*int[][] zadanie6Tablica = new int [5][5];
        for (int i = 0; i < zadanie1Tablica.length; i++){
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
                zadanie6Tablica[i][j] = zadanie1Tablica[j][i];
            }
        }
        System.out.println("");
        System.out.println("Zamiana wierszy z kolumanmi");
        wypisz(zadanie6Tablica);
        */
        /// zadanie 7 //////////////////////////////////////////////////////

       /* int [] tablicaJednowymiarowa = new int [zadanie1Tablica.length];
        for (int i = 0; i < zadanie1Tablica.length; i++){
            int suma7 = 0;
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
               tablicaJednowymiarowa[i] = tablicaJednowymiarowa[i] + zadanie1Tablica[i][j];
            }


        }
        System.out.println("Suma po wierszach");
        wypisz(tablicaJednowymiarowa);*/

        // zadanie 8 /////////////////////////////////////////////////////////////


        /*int [] tablicaJednowymiarowa2 = new int [zadanie1Tablica.length];
        for (int i = 0; i < zadanie1Tablica.length; i++){
            int suma7 = 0;
            for (int j = 0; j < zadanie1Tablica[i].length; j++){

                tablicaJednowymiarowa2[j] = tablicaJednowymiarowa2[j] + zadanie1Tablica[i][j];
            }


        }
        System.out.println("Suma po kolumanch");
        wypisz(tablicaJednowymiarowa2);*/

        // Zadanie 9 //////////////////////////////////////////////////////////////////

            /*int suma9 = 0;
        for (int i = 0; i < zadanie1Tablica.length; i++){
            int suma7 = 0;
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
                if(i ==j){
                    suma9 = suma9 + zadanie1Tablica[i][j];
                }
            }
        }
        System.out.println("Suma po diagonalnej");
        System.out.println(suma9);*/
        // zadanie 10 ////////////////////////////////////////////////////////////////////

        int suma10 = 0;
        for (int i = 0; i < zadanie1Tablica.length; i++){
            int suma7 = 0;
            for (int j = 0; j < zadanie1Tablica[i].length; j++){
                if(i + j == zadanie1Tablica.length -1  ){
                    //System.out.println("wchodze");
                    suma10 = suma10 + zadanie1Tablica[i][j];
                }
            }
        }
        System.out.println("Suma po Anty-diagonalnej");
        System.out.println(suma10);


    }




    static void wypisz(int[][] tablica) {
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[i].length; j++) {
                System.out.print(tablica[i][j] + " ");
            }
            System.out.println("");
        }
    }

    static void wypisz(int[] tablica) {
        for (int i = 0; i < tablica.length; i++) {

                System.out.print(tablica[i] + " ");
            }
            System.out.println("");
        }



    static int[][] generujTabliceInt(int wiersz, int kolumna) {
        int[][] tablica = new int[wiersz][kolumna];
        Random random = new Random();
        for (int i = 0; i < tablica.length; i++) {
            for (int j = 0; j < tablica[i].length; j++) {
                tablica[i][j] = random.nextInt(10);
            }
        }
        return tablica;
    }

}

