package ZadanieDodatkowe5;

public class Main {
    public static void main(String[] args) {
        int [] tablica = {1,2,3,4};
        System.out.println(konwersja(tablica));
    }
    public static int konwersja(int[] tablica ){
        int suma = 0;
        for(int i = 0 ; i < tablica.length; i++){
            suma = suma + tablica[i] * (int)Math.pow(10, tablica.length - i - 1);
        }
        return (int)suma + 1 ;
    }
}
