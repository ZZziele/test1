package zadanie3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int a,b,c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj wsp a");
        a = scanner.nextInt();
        System.out.println("Podaj wsp b");
        b = scanner.nextInt();
        System.out.println("Podaj wsp c");
        c = scanner.nextInt();

        System.out.println("Równianie:  " + a + "x^2 + " + b + "x + " + c + "= 0");
        double delta = Math.pow(b,2) - 4*a*c;

        if(delta < 0 ){
            System.out.println("Brak miejsc zerowych");
        }else if(delta == 0){
            double x0 = -b/(2*(double)a);
            System.out.println("jedno miejsce zerowe: " + x0);
        }else {
            double x1 = (-b-Math.sqrt(delta)) /(2*(double) a);
            double x2 = (-b+Math.sqrt(delta)) /(2*(double) a);
            System.out.println("Dwa miejsca zerowe: ");
            System.out.println("x1 = " + x1 );
            System.out.println("x2 = " + x2 );
        }

    }
}
