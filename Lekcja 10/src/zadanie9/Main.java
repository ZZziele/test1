package zadanie9;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.print("Podaj Dlugosc");
        Scanner scanner = new Scanner(System.in);
        int amplitude = scanner.nextInt();

        for(int j = 0 ; j < 4; j++) {
            for (int i = 0; i < amplitude; i++) {
                if (i % 8 == (j) || i % 8 == (7 -j)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }
}
