package zadanie17;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("poda date następnych zajęć (rok miesiac dzień)");
        String data1 = scanner.next();
        LocalDate data = LocalDate.parse(data1);
        System.out.println("Ilosc dni  " + Period.between(LocalDate.now(), data).getDays() + "miesiecy: " + Period.between(LocalDate.now(), data).getMonths() + " lat " + Period.between(LocalDate.now(), data).getYears());


    }
}
