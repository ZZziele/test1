package zadanie16;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [] tablica = new int[10];

        for(int i = 0; i < tablica.length; i++) {
            tablica[i] = scanner.nextInt();
        }

        System.out.println(ciag(tablica));
    }

    public static int ciag(int[] tablica) {
        int max = 1 ,licznik = 1;
        for (int i = 0; i < tablica.length - 1 ; i++) {
            if (tablica[i]<+ tablica[i+1]) {
                licznik++;
                if (licznik > max) max = licznik;
            }
            else licznik = 1;
        }
        return max;
    }

}
