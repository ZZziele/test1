package zadanie7;

import java.util.Scanner;
import java.util.SortedMap;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj liczne n");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        System.out.println("" + n +" liczba w ciagu fibonaciego jest: " +fibonaci(n));
    }
    static int fibonaci(int n){
        int suma = 0;
        if(n == 2 || n == 1 ){
            return 1;
        }
        suma = fibonaci(n-1) + fibonaci(n-2);
        return suma ;
    }
}