package zadanie6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj liczbe n");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double suma = 0;
        for(int i =1 ;i <= n; i++ ){
            suma = suma + 1/(double)i;
        }
        System.out.println("Suma = " + suma);
    }
}