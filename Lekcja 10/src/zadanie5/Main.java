package zadanie5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Podaj liczbe całkowita dodatnia");
        Scanner scanner = new Scanner(System.in);
        int liczba = scanner.nextInt();
        for(int i = 2 ;i < liczba; i++){
            boolean czyPierwsza = true;
            for (int j = 2; j < i; j++){
                if(i%j==0){
                    czyPierwsza = false;
                    break;
                }
            }
            if(czyPierwsza){
                System.out.println(i);
            }
        }
    }
}