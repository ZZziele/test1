package zadanie19;

public class Main {
    public static void main(String[] args) {

        Author  author1 = new Author("Słowacki", "Poland");
        Author  author2 = new Author("Rowling", "England");
        Author  author3 = new Author("Sanderson", "USA");
        Poem poem1 = new Poem(author1,44);
        Poem poem2 = new Poem(author2,55);
        Poem poem3 = new Poem(author3,33);
        Poem[] tablica = new Poem[] {poem1,poem2,poem3};

        System.out.println("Autorem nadjłuższego dzieła jest  " + bestAuthor(tablica));
    }
    public static String bestAuthor (Poem[] tablica){
        int indeks = 0 , max = 0;

        for(int i = 0 ; i < tablica.length ;i++) {
            if(tablica[i].getStropheNumbers() > max) {
                max = tablica[i].getStropheNumbers();
                indeks = i;
            }
        }
        return tablica[indeks].getAutor().getSurname();
    }
}
