package zadanie19;

public class Poem {
    private Author autor;

    private int stropheNumbers;

    public Author getAutor() {
        return autor;
    }

    public void setAutor(Author autor) {
        this.autor = autor;
    }

    public int getStropheNumbers() {
        return stropheNumbers;
    }

    public void setStropheNumbers(int stropheNumbers) {
        this.stropheNumbers = stropheNumbers;
    }

    public Poem(Author autor, int stropheNumbers) {
        this.autor = autor;
        this.stropheNumbers = stropheNumbers;
    }
}
