package ZadanieDodatkowe3;

public class Main {
    public static void main(String[] args) {
        int[] tablica = {10,20,0,0,10,20,30,80};
        int numberOfDuplicate = 0;

        for(int i = 0; i < tablica.length; i++){
            for(int j = 0; j < i; j++){
                if(tablica[i] == tablica[j]){
                    numberOfDuplicate++;
                }
            }
        }

        int[] tablica2 = new int[tablica.length-numberOfDuplicate];
        int k =0;

        for(int i = 0; i < tablica.length; i++){
            boolean czySamotna = true;
            for(int j = 0; j < i; j++){
                if(tablica[i] == tablica[j]){
                    czySamotna = false;
                }
            }
            if(czySamotna) {
                tablica2[k] = tablica[i];
                k++;
            }
        }


        for(int i = 0; i < tablica2.length; i++){
            System.out.println(tablica2[i]);
        }
    }
}
