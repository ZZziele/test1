package ZadanieDodatkowe2;
//          Mając listę liczb i liczbę k, zwróć, czy dowolne dwie liczby z listy sumują się do k.
  //      Na przykład, biorąc pod uwagę [10, 15, 3, 7] i k z 17, zwróć prawdę, ponieważ 10 + 7 to 17. Nie używaj Kolekcji (Collections - List, Setów itp.).
public class Main {
    public static void main(String[] args) {
        int [] tablica = {2,5,7,9,11,54,23,10};
        int liczba = 35;
        boolean flaga = false;
        for(int i = 0; i < tablica.length; i++){
            for(int j=0; j<tablica.length; j++){
                if(i != j && tablica[i]+tablica[j]==liczba) {
                    flaga = true;
                }
            }
        }
        System.out.println(flaga);
    }
}
